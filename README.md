Se realiza ajustes a esquema de selección de items para tienda virtual MEGAMODA, pues se desea que al seleccionar una opción sean mostrados los datos en tiempo de ejecución correspondientes. 
También se implementa carro de compra con MercadoPago.

El requerimiento era cambiar el modo de selección de las opciones para la compra de un producto y que permitiera "comprar" sin necesidad de haber iniciado sesión en el sitio. Es decir, poder cargar items al carro de forma offline y ya cuando el cliente deseara finalizar la compra, darle clic al "carro" luego a la opción "Comprar" y con ello lleva a la pasarela de pago. 

Para solucionar el caso de cargar items de forma offlinea, se usaron variables de sesión y se cambia el flujo de la compra. Adicional, se agrega el módulo de la pasarela de pago.

Este trabajo de corto plazo fue realizado trabajando para la compañía de Proyecto Kamila