<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Item Entity.
 *
 * @property int $id
 * @property int $art_cod
 * @property string $title
 * @property string $descripcion
 * @property string $marca
 * @property int $sub_category_id
 * @property \App\Model\Entity\SubCategory $sub_category
 * @property string $aux1
 * @property string $aux2
 * @property string $aux3
 * @property string $aux4
 * @property string $aux5
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Combination[] $combinations
 * @property \App\Model\Entity\Price[] $prices
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    protected function _getCategory()
    {
        $category = TableRegistry::get('Categories');
        
        $category = $category->find()->select()->where(['id' => $this->sub_category->category_id])->first();  
        if ($category) {
            return $category->title;
        } else {
            return "---";
        }
    }
    
    protected function _getPrice()
    {
         //busco el precio adecuado
        
        $prices = TableRegistry::get('Prices');
        
        $price = $prices->find()->select()->where(['item_id' => $this->id])->order(['created' => 'DESC'])->first();  
        
        if($price)
        {
            $oferta = $price->precio_oferta!=0  && ($price->vigencia_oferta >= Time::now());
            if($oferta)
                return $price->precio_oferta;        
            else
                return $price->precio_lista;                    
        }
        else
            return null;
    }   
    
    protected function _getListprice()
    {
         //busco el precio adecuado
        
        $prices = TableRegistry::get('Prices');
        
        $price = $prices->find()->select()->where(['item_id' => $this->id])->order(['created' => 'DESC'])->first();  
        
        if($price)
        {
            return $price->precio_lista;                    
        }
        else
            return null;
    } 
    
    protected function _getOferta()
    {
        $prices = TableRegistry::get('Prices');
        
        $price = $prices->find()->select()->where(['item_id' => $this->id])->order(['created' => 'DESC'])->first();  
        
        if($price)
        {
            return $price->precio_oferta!=0  && ($price->vigencia_oferta >= Time::now());                  
        }
        else
            return null;
    }    
}
