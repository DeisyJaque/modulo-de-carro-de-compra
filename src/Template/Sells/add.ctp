<div class="actions columns col-lg-2 col-md-3 col-xs-12 dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >    
        <?= __('Actions') ?>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li class="active"><?= $this->Html->link(__('New Sell'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sells'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sell Items'), ['controller' => 'SellItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sell Item'), ['controller' => 'SellItems', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="sells form col-lg-10 col-md-9 col-xs-12 columns">
    <?= $this->Form->create($sell); ?>
    <fieldset>
        <legend><?= __('Add Sell') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('cart');
        ?>
    </fieldset>
    
    <div class=text-right>
        <?= $this->Form->button(__('Submit'), ['class' => 'btn-success']) ?>
    </div>
    
    <?= $this->Form->end() ?>
</div>
