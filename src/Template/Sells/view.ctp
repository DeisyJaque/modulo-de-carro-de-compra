<div class="actions columns col-lg-2 col-md-3 col-xs-12 dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >    
        <?= __('Actions') ?>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><?= $this->Html->link(__('Edit Sell'), ['action' => 'edit', $sell->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sell'), ['action' => 'delete', $sell->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sell->id), 'class' => 'btn-danger']) ?> </li>
        <li><?= $this->Html->link(__('List Sells'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sell'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sell Items'), ['controller' => 'SellItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sell Item'), ['controller' => 'SellItems', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="sells view col-lg-10 col-md-9 columns">
    <h2><span class="label label-primary"><?= h($sell->id) ?></span></h2>
    <div class="row">
        <div class="col-lg-5 columns strings">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <span class="label label-primary"><?= __('User') ?></span>
                    <p><?= $sell->has('user') ? $this->Html->link($sell->user->id, ['controller' => 'Users', 'action' => 'view', $sell->user->id]) : '' ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-2 columns numbers end">
            <div class="panel panel-info">
                <div class="panel-body">
                    <span class="label label-info"><?= __('Id') ?></span>
                    <p><?= $this->Number->format($sell->id) ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-2 columns dates end">
            <div class="panel panel-success">
                <div class="panel-body">
                    <span class="label label-success"><?= __('Created') ?></span>
                    <p><?= h($sell->created) ?></p>
                    <span class="label label-success"><?= __('Modified') ?></span>
                    <p><?= h($sell->modified) ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-2 columns booleans end">
            <div class="panel panel-warning">
                <div class="panel-body">
                    <span class="label label-warning"><?= __('Cart') ?></span>
                    <p><?= $sell->cart ? __('Yes') : __('No'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column col-lg-12">
    <h4 class="subheader"><?= __('Related SellItems') ?></h4>
    <?php if (!empty($sell->sell_items)): ?>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cantidad') ?></th>
                <th><?= __('Precio') ?></th>
                <th><?= __('Combination Id') ?></th>
                <th><?= __('Sell Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sell->sell_items as $sellItems): ?>
            <tr>
                <td><?= h($sellItems->id) ?></td>
                <td><?= h($sellItems->cantidad) ?></td>
                <td><?= h($sellItems->precio) ?></td>
                <td><?= h($sellItems->combination_id) ?></td>
                <td><?= h($sellItems->sell_id) ?></td>
                <td><?= h($sellItems->created) ?></td>
                <td><?= h($sellItems->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<span class="glyphicon glyphicon-zoom-in"></span><span class="sr-only">' . __('View') . '</span>', ['controller' => 'SellItems', 'action' => 'view', $sellItems->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('View')]) ?>
                    <?= $this->Html->link('<span class="glyphicon glyphicon-pencil"></span><span class="sr-only">' . __('Edit') . '</span>', ['controller' => 'SellItems', 'action' => 'edit', $sellItems->id], ['escape' => false, 'class' => 'btn btn-xs btn-default', 'title' => __('Edit')]) ?>
                    <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span><span class="sr-only">' . __('Delete') . '</span>', ['controller' => 'SellItems', 'action' => 'delete', $sellItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sellItems->id), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
    </div>
</div>
