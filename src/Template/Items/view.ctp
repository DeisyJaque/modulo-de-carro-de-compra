<?php
    $this->Html->script('ItemsManager', ['block' => true]); //block = true es para que lo levante en el layout con this->fetch('script')
?>

<script type="text/javascript"> var baseUrl = "<?php echo $this->Url->build('/'); ?>"</script>

<div id="productos-header" class="categories index col-lg-12 col-md-12 col-xs-12 columns">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Productos'),'/categorias'); ?></li>
        <li><?php echo $this->Html->link($category,array('controller'=>'categories','action'=>'view',$item->sub_category->category_id)); ?></li>
        <li><?php echo $this->Html->link($item->sub_category->title,array('controller'=>'SubCategories','action'=>'view',$item->sub_category->id)); ?></li>
        <li class="active"><?php echo $this->Html->link($item->title,array('controller'=>'items','action'=>'view',$item->id)); ?></li>
    </ol>
</div>

<div class="row">
<div id="producto" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <br>
    <div id="producto-imagenes" class="col-xs-12  col-sm-12  col-md-6 col-lg-6">
        <?php   
            if(!empty($item->path_img))
            {

                echo "<div class='marco'>";
                echo $this->Html->image(h($item->path_img), array('alt'=>"foto item", 'border'=>"0", 'class'=>"img-responsive center-block",'id'=>"img-prod"));
                echo "</div>";
                echo"&nbsp";
            }
        ?>  
        
        <div id="producto-thmbs" class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="prod-thumb1" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?php   
                if(!empty($item->path_img2))
                {

                    echo "<div class='marco'>";
                    echo $this->Html->image(h($item->path_img2), array('alt'=>"foto item", 'border'=>"0", 'class'=>"img-responsive center-block",'id'=>"img-thumb1-prod"));
                    echo "</div>";
                    echo"&nbsp";
                }
            ?> 
            </div>
            <div id="prod-thumb2" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?php   
                if(!empty($item->path_img3))
                {

                    echo "<div class='marco'>";
                    echo $this->Html->image(h($item->path_img3), array('alt'=>"foto item", 'border'=>"0", 'class'=>"img-responsive center-block",'id'=>"img-thumb2-prod"));
                    echo "</div>";
                    echo"&nbsp";
                }
            ?>                  
            </div>
            <div id="prod-thumb3" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?php   
                if((!empty($item->path_img)) && ( (!empty($item->path_img2)) || (!empty($item->path_img3)) ) )
                {

                    echo "<div class='marco'>";
                    echo $this->Html->image(h($item->path_img), array('alt'=>"foto item", 'border'=>"0", 'class'=>"img-responsive center-block",'id'=>"img-thumb3-prod"));
                    echo "</div>";
                    echo"&nbsp";
                }
            ?>                  
            </div>
        </div>
   
        <script>
           $(document).ready(function() 
            {   
                //TODO mobile work
                
                $('#prod-thumb1').on('click',
                    function(event){
                        event.preventDefault();
                        $('#img-prod').fadeOut(400, function() {$('#img-prod').attr('src',$('#img-thumb1-prod').attr('src'))}).fadeIn(400);
                        return false;
                    }
                );        
        
                $('#prod-thumb2').on('click',
                    function(){                        
                        $('#img-prod').fadeOut(400, function() {$('#img-prod').attr('src',$('#img-thumb2-prod').attr('src'))}).fadeIn(400);                        
                    }
                );        
        
                $('#prod-thumb3').on('click',
                    function(){
                        $('#img-prod').fadeOut(400, function() {$('#img-prod').attr('src',$('#img-thumb3-prod').attr('src'))}).fadeIn(400);
                    }
                );        
            });        
        </script>
    </div>
    
    <div id="producto-detalles" class="col-xs-12  col-sm-12  col-md-6 col-lg-6">
        <div id="ver-nombre-articulo"><p><?= h($item->title) ?></p></div>
        <div id="ver-descrip-articulo"><p><?= $this->Text->autoParagraph(h($item->descripcion)); ?></p></div>        
        <div id="ver-precio-articulo"><p><del class="text-muted"><?php if(isset($listprice) && isset($oferta) && $oferta) echo "$ ".$listprice ?></del> <?php if(isset($price)) echo "$ ".$price ?> <?php if(isset($oferta) && $oferta) echo " - ¡OFERTA!" ?></p></div>        
    </div>    
    
</div>
</div>


<div class="related row">
    <div class="column col-lg-12">
    <h4 class="subheader"></h4>
    <?php if (!empty($item->combinations)): ?>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <tr>
                <th><?= __('Talle') ?></th>
                <th><?= __('Color') ?></th>
                <th><?= __('Stock') ?></th>
                <th><?= __('Cantidad') ?></th>
            </tr>
            <?php foreach ($item->combinations as $combinations): ?>
                <?php if ($combinations->stock > 0): ?>
                    <tr>
                        <td><?= h($combinations->talle) ?></td>
                        <td><?= h($combinations->color) ?></td>
                        <td><?= h($combinations->stock) ?></td>
                        <td class="actions">
                            <input id="input-quantity<?=$combinations->id?>" class="input-text qty input-quantity" type="text" name="qty" value="1">                
                            <?php echo '<a class= "add-to-cart btn btn-xs btn-danger" id=compra'.$combinations->id.'> <span class="glyphicon glyphicon-shopping-cart"> Comprar </span><span class="sr-only">' . __('Comprar') . '</span>' 
                            ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
    </div>
</div>

