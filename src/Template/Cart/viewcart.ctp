
<div id="productos-header" class="categories index col-lg-12 col-md-12 col-xs-12 columns">
    <?= __('MI COMPRA: ')?>
</div>

    <?php echo $this->element('compra');?>

<div class="row">
    
<div class="col-xs-12 text-right">    
    <div id="cart-check-out" class=" col-xs-12 ">
        <a href="<?php echo $this->Url->build('/cart/checkout', true); ?>" class="btn btn-lg btn-success">Comprar <span class="glyphicon glyphicon-shopping-cart"></span></a>
    </div> 
</div>
</div>

<?php
    $this->Html->script('CartManager.js', ['block' => true]);
?>