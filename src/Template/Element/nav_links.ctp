<ul id="navlinks" class="nav navbar-nav" > 

    <li class="toolbar_button" id="toolbar_inicio">

<!--        ATENCION los links deberían ser de esta forma:

            echo $this->Html->link(__('Inicio'), array('controller'=>'pages','action'=>'inicio'));

            pero como tengo rutas definidas en el archivo routes las hago mas simples, solo apra que quede mejor desde el punto de vista SEO y URL friendly-->

            <!--class="active"  en el li para hacerlo activo a ese link, o sea que sea el seleccionado-->

        <?php echo $this->Html->link(__('Inicio'), '/'); ?>
    </li>

    <li class="toolbar_button" id="toolbar_productos">
        <?php echo $this->Html->link(__('Productos'),'/categorias'); ?>
    </li>

    <li class="toolbar_button" id="toolbar_novedades">
        <?php echo $this->Html->link(__('Ofertas'), '/novedades'); ?>
    </li> 

    <li class="toolbar_button" id="toolbar_contacto">
        <?php echo $this->Html->link(__('Contacto'),'/contacto'); ?>
    </li>        

</ul>   