<div id="footer" class="wide container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
    
    <div id="footer-up" class="wide container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row col-xs-10 col-xs-offset-1
                    col-sm-10 col-sm-offset-1 
                    col-md-8 col-md-offset-2
                    col-lg-8 col-lg-offset-2 
                    text-center">
            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <a href="<?php echo $general_footer_facebook_link ?> ">
                <i class="glyphicon glyphicon-thumbs-up" id="carro-cant"> 
                <span>
                <?php   
                    if(!empty($general_footer_facebook_link))
                    {
                        echo "/megamodaindumentaria";
                    }
                ?>     
                </span>
                </i>
            </a>    
            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <span>
                <i class="glyphicon glyphicon-earphone" id="carro-cant">
                <span>
                <?php   
                    if(!empty($general_footer_tel_data))
                    {
                        echo $general_footer_tel_data;
                    }
                ?>
                </span>
                </i>
            </span> 
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <a href="mailto:<?php echo $general_email_data ?> ">
                <i class="glyphicon glyphicon-envelope" id="carro-cant">
                 <span>
                <?php   
                    if(!empty($general_email_data))
                    {
                        echo $general_email_data;
                    }
                ?>   
                </span>
                </i>
            </a>  
            </div>
            
        </div>
    </div>

    <div id="footer-down" class="wide container-fluid  col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div>
            
                <?php   
                    if(!empty($general_footer_texto))
                    {
                        echo $general_footer_texto;
                    }
                ?>               
            
        </div>
    </div>    

</div>
    
 
    