<?php

if (!empty($cart->sell_items)): ?>

<style>
    table {font: 15px arial, sans-serif;}
    p {font: 15px arial, sans-serif;}
    tbody tr:nth-child(even) {background-color: #f2f2f2}
</style>
<div>
    <table style="width: 100%;">
        <thead>
            <tr style="background-color: #ccc;">
                <th style="text-align: center; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Referencia</th>
                <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Producto</th>
                <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Talle</th>
                <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Color</th>
                <th style="text-align: right; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Precio unitario</th>
                <th style="text-align: right; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Cantidad</th>
                <th style="text-align: right; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">Precio Total</th>
            </tr>
        </thead>
        <tbody>
        <?php $total=0;?>
            <?php foreach ($cart->sell_items as $sellItem): ?>
            <tr style="padding-top: 2px; padding-bottom: 2px;">
                <td style="text-align: center; padding-left: 5px; padding-right: 5px;"><?= h($sellItem->combination->item->art_cod)?></td>
                <td style="padding-left: 5px; padding-right: 5px;"><?= h($sellItem->combination->item->title)?></td>
                <td style="padding-left: 5px; padding-right: 5px;"><?= h($sellItem->combination->talle) ?></td>
                <td style="padding-left: 5px; padding-right: 5px;"><?= h($sellItem->combination->color) ?></td>
                <td style="text-align: right; padding-left: 5px; padding-right: 5px;"><?= '$'.h($sellItem->precio)?></td> 
                <td style="text-align: right; padding-left: 5px; padding-right: 5px;"><?= h($sellItem->cantidad) ?></td>
                <td style="text-align: right; padding-left: 5px; padding-right: 5px;"><?php echo"$"; echo($sellItem->precio*$sellItem->cantidad); $total = $total+($sellItem->precio*$sellItem->cantidad); ?></td>
            </tr>
        <?php endforeach; ?>
            <tr style="background-color: #bd1622;">
                <td style="text-align: right; color: white; padding-top: 10px; padding-bottom: 10px; font-weight: 800;" colspan=6>Total</td>
                <td style="text-align: right; color: white; padding-top: 10px; padding-bottom: 10px; font-weight: 800;">$ <?php echo $total; ?></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 100%;">
        <thead>
            <tr>
                <th colspan=2 style="background-color: #ccc; text-align: left;">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Nombre y apellido</td>
                <td><?php echo $user->nombre . " " . $user->apellido; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?php echo $user->email;?></td>
            </tr>
            <tr>
                <td>Tel&eacute;fono</td>
                <td><?php echo $user->tel;?></td>
            </tr>
            <tr>
                <td>Direcci&oacute;n</td>
                <td><?php echo $user->direccion; ?></td>
            </tr>
            <tr>
                <td>Localidad</td>
                <td><?php echo $user->localidad; ?></td>
            </tr>
            <tr>
                <td>CP</td>
                <td><?php echo $user->cp; ?></td>
            </tr>
            <tr>
                <td>Provincia</td>
                <td><?php echo $user->provincia; ?></td>
            </tr>
            <tr>
                <td>Pa&iacute;s</td>
                <td><?php echo $user->pais; ?></td>
            </tr>
            <tr>
                <td>Fecha de la orden</td>
                <td><?php echo $fecha; ?></td>
            </tr>
        </tbody>
    </table>
    <p></p>
</div>
<?php endif; ?>

