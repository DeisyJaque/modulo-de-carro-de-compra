<div id="barra_busqueda" class="barra_b col-md-12" >
    
        <div class="col-sm-12 col-md-12 text-center">
            
            <?php // aca necesito que el buscar me forme una URL asi http://localhost/cakePHP/megamoda/pages/search?s=remera ?>
            
        <form class="navbar-form" role="search">
            <div class="input-group">
                <input id="srch-term" type="text" class="form-control " placeholder="Buscar" name="srch-term">
                <div class="input-group-btn">
                    <button id='form-search-button' class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
            
            <a href="<?php echo $this->Url->build('/cart', true); ?>" id="cart-link">
                <i class="glyphicon glyphicon-shopping-cart" id="carro-cant"> <?php if(isset($cartcant)){echo "<span id='cart-cant' class='badge cart-badge'>".$cartcant."</span>";}else{echo "<span id='cart-cant' class='badge'>0</span>";}?>
                </i>
            </a>    
               
            
            <?php if(isset($username)): ?>
                <a href="<?php echo $this->Url->build('/users/edit/'.$userid, true); ?>">
                    <i class="user-logged glyphicon glyphicon-user">    
                        <?php echo $username;?>
                    </i>
                </a>    
                <a href="<?php echo $this->Url->build('/logout', true); ?>">
                    <i class="glyphicon glyphicon-log-out">    
                        <?php echo "cerrar sessión" ?>
                    </i>
                </a>               
            <?php else: ?>
                <a href="<?php echo $this->Url->build('/login', true); ?>">
                    <i class="glyphicon glyphicon-user">    
                        <span><?php echo "Ingresar-Registrase";?></span>
                    </i>
                </a>                          
            <?php endif; ?>
            
        </div>
    
</div>
<script> var baseUrl = "<?php echo $this->Url->build('/', true);?>"</script>
  
