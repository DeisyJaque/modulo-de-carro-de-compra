<ul id="navlinks" class="nav navbar-nav" > 

    <li class="toolbar_button" id="toolbar_inicio">

<!--        ATENCION los links deberían ser de esta forma:

            echo $this->Html->link(__('Inicio'), array('controller'=>'pages','action'=>'inicio'));

            pero como tengo rutas definidas en el archivo routes las hago mas simples, solo apra que quede mejor desde el punto de vista SEO y URL friendly-->

            <!--class="active"  en el li para hacerlo activo a ese link, o sea que sea el seleccionado-->

        <?php echo $this->Html->link(__('Home'), '/'); ?>
    </li>

    <li class="toolbar_button" id="toolbar_productos">
        <?php echo $this->Html->link(__('Productos'),'/admin/items'); ?>
    </li>

    <li class="toolbar_button" id="toolbar_productos">
        <?php echo $this->Html->link(__('Categorias'),'/admin/categories'); ?>
    </li>

    <li class="toolbar_button" id="toolbar_novedades">
        <?php echo $this->Html->link(__('Ventas'), '/admin/sells'); ?>
    </li> 

    <li class="toolbar_button" id="toolbar_contacto">
        <?php echo $this->Html->link(__('Configuraciones'),'/admin/configurations'); ?>
    </li>        

    <li class="toolbar_button" id="toolbar_contacto">
        <?php echo $this->Html->link(__('Slider'),'/admin/slides'); ?>
    </li> 

    <li class="toolbar_button" id="toolbar_contacto">
        <?php echo $this->Html->link(__('Usuarios'),'/admin/users'); ?>
    </li> 

</ul>   