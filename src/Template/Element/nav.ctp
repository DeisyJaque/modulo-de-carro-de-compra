<nav id="" class="navbar navbar-default navmegamoda">        
    
    <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
        <?php 
            if($authUser)
            {
              echo $this->element('nav_links_admin'); 
            }
            else
            {
              echo $this->element('nav_links');  
            }
        ?>
      </div><!-- /.navbar-collapse -->
    </div>
  
</nav>    
