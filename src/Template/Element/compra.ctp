<div class="related row">
    <div class="column col-lg-12">
        <div class='row'>
            <div class="column col-lg-3">
                <h4 class="subheader"><?= __('Cart items') ?></h4>
            </div>
        </div>
        
       
        <?php if (!empty($cart['sell_items'])): ?>
    <div class="table-responsive">
        <table id='cart-items-table' class="table table-striped table-hover">
            <tr>
                <th><?= __('item Name') ?></th>
                <th><?= __('Talle') ?></th>
                <th><?= __('Color') ?></th>
                <th><?= __('Stock') ?></th>
                <th><?= __('Precio') ?></th>
                <th><?= __('Cant.') ?></th>
                <th><?= __('Subtotal') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            
            <?php $total=0;?>
            <?php foreach ($cart['sell_items'] as $sellItem): ?>
            <tr>
                <td><?= h($sellItem['combination']['item']['title'])?></td>
                <td><?= h($sellItem['combination']['talle']) ?></td>
                <td><?= h($sellItem['combination']['color']) ?></td>
                <td class='stock-col'><?= h($sellItem['combination']['stock'])//TODO mostar el stock del item?></td>
                <td><?= '$'.h($sellItem['precio'])?></td> 
                <td class='cant-col'><?= h($sellItem['cantidad']) ?></td>
                <td><?php echo"$"; echo($sellItem['precio']*$sellItem['cantidad']); $total = $total+($sellItem['precio']*$sellItem['cantidad']); ?></td>
                <td class="actions">
                    <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash"></span><span class="sr-only">' . __('Delete') . '</span>', ['controller' => 'Sell_items', 'action' => 'delete', $sellItem['id']], ['confirm' => __('Are you sure you want to delete # {0}?', $sellItem['id']), 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'title' => __('Delete')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
    </div>
</div>

<div class="row">
<div class="col-xs-12 text-right">
    <div id="total-cart" class=" col-xs-12"><h2> TOTAL: $ <?=$total?> </h2></div> 
</div>
</div>

