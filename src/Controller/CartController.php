<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Exception\Exception; // para usar la ex
use Cake\Network\Email\Email; // para mandar el mail
use Cake\Datasource\ConnectionManager;
use MP;


/**
 * Combinations Controller
 *
 * @property \App\Model\Table\CombinationsTable $Combinations
 */
class CartController extends AppController
{
    
    /**
     * El manejo del carro de compras,
     * en este caso utilizo una venta de la tabla ventas, solo que con el atributo cart en true
     * eso significa qeu todavia no es una venta concretada y que solo puede existir un cart por usuario
     * cuando se ejecute el checkout del cart se concreta la venta y se descarga del stock
     */
   
   
    //add to cart
    public function addtc($combinationId = null, $cant = null) //add to cart
    {
        
        $session = $this->request->session();
        $session->write('combination_id', $combinationId); 
        $session->write('combination_cant', $cant); 
        
        
        if($this->Auth->user()!= null)
        {
            //Fijarse si el usuario ya tiene una venta usada como carro de compras, es decir con cart en true
            // es importante que solo hay una con cart en true para cada usuario
            
            $user_id = $this->Auth->user('id');
            
                     
            
            $this->loadModel('Sells');
            
           
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id , 'cart' => true])->all();
            
       

            $cantSells = count($sells);
           
            $sell=null;
            
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
            }
            else
            {
                if($cantSells==0)
                {
                    //Si no se encuentra una venta con cart en true, se crea una nueva  
                    $sellsTable = TableRegistry::get('Sells');
                    $sell = $sellsTable->newEntity();
                    $sell->user_id = $user_id;
                    $sell->cart=true;
                     $this->Sells->save($sell);
                    //para que genere el id automáticamente, de lo contrario, queda mal cuando se asigne al sell items
                   
                    
                }
                else if ($cantSells==1)
                {
                    //De haber una, se usa ese
                    $sell=$sells->first();
                     
                }                                
               
                //Ya se tiene la venta con la que se desea trabajar, entonces se le agrega el item a esa venta
                //En realidad se está agregando al carro
                
                
                
                $this->loadModel('Sell_items');
                $sell_itemsTable = TableRegistry::get('Sell_items');
                $sell_item = $sell_itemsTable->newEntity();
                
                $sell_item->cantidad = $cant;
                
                $this->loadModel('Combinations');
                $combination = $this->Combinations->get($combinationId);
                
                $this->loadModel('Items');
                $item = $this->Items->get($combination->item_id);
                
                $sell_item->precio = $item->price; 
                
                $sell_item->combination_id = $combinationId;
                $sell_item->sell_id = $sell->id;
              
                
                //Guardar la venta y el item de ella
                $this->Sells->save($sell);
                $this->Sell_items->save($sell_item);

                //Luego se tiene que ver como incrementar un item en el carro de compras y a que vista redirige

                
                
                $this->Flash->success(__('Se ha agregado este producto a su carro de compras!'));
            
            }
            
            return $this->redirect($this->referer());  
        }
        else
        {    
            $session = $this->request->session();
            
            if($session->check('cart')){
                $cart = $session->read('cart'); 
            }
            
      
               
            $cart[] = ['ID' => $combinationId, 'cant' => $cant];
            $session->write('cart',$cart);
            
            $this->Flash->success(__('Se ha agregado este producto a su carro de compras!'));
          
          
            return $this->redirect($this->referer());  
        }
      
    }   
    //add to cart
    public function addtccopia($combinationId = null, $cant = null) //add to cart
    {
     
       $session = $this->request->session();
       $session->write('combination_id', $combinationId); 
        $session->write('combination_cant', $cant); 
        
        
        if($this->Auth->user()!= null)
        {
            //Fijarse si el usuario ya tiene una venta usada como carro de compras, es decir, con cart en true
            //Es importante que solo hay una con cart en true para cada usuario
            
            $user_id = $this->Auth->user('id');
            
               
            
            $this->loadModel('Sells');
            
          
            
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id , 'cart' => true])->all();
            
            

            $cantSells = count($sells);
           
            $sell=null;
            
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
            }
            else
            {
                if($cantSells==0)
                {
                    //Si no se encuentra una venta con cart en true, se crea una nueva  
                    $sellsTable = TableRegistry::get('Sells');
                    $sell = $sellsTable->newEntity();
                    $sell->user_id = $user_id;
                    $sell->cart=true;
                     $this->Sells->save($sell);
                    //Para que genere el id automáticamente, de lo contrario, queda mal cuando se asigna al sell items
                   
                    
                }
                else if ($cantSells==1)
                {
                    //De haber una, se usa
                    $sell=$sells->first();
                     
                }                                
               
                //Ya se tiene la venta deseada para trabajar, entonces se le agrega el item a esa venta
                //En realidad se está agregando al carro
                
                
                
                $this->loadModel('Sell_items');
                $sell_itemsTable = TableRegistry::get('Sell_items');
                $sell_item = $sell_itemsTable->newEntity();
                
                $sell_item->cantidad = $cant;
                
                $this->loadModel('Combinations');
                $combination = $this->Combinations->get($combinationId);
                
                $this->loadModel('Items');
                $item = $this->Items->get($combination->item_id);
                
                $sell_item->precio = $item->price; 
                
                $sell_item->combination_id = $combinationId;
                $sell_item->sell_id = $sell->id;
              
                
                //Guardar la venta y el item de ella
                $this->Sells->save($sell);
                $this->Sell_items->save($sell_item);

              
                
                $this->Flash->success(__('Se ha agregado este producto a su carro de compras!'));
            }
            
            return $this->redirect($this->referer());  
        }
        else
        {    
            
              $sessionuser = $this->request->session();
            
             if(!$sessionuser->check('id')){
                 $this->Flash->success(__($user_id));
              $this->loadModel('Users');
              $user= $this->Users->find()->order([ 'id' => 'DESC'])->first();
             $user_id=($user->id)+1;
             $sessionuser->write('id',$user_id); 
             
              $userentity = TableRegistry::get('Users');
              $usernew = $userentity->newEntity(); 
                $usernew->id = $user_id;
                $usernew->nro_cliente=null;
                $usernew->username='rdasdandom';
                $usernew->password='dasdadasdasd';
                $usernew->role='otro';
                $usernew->nombre='jaisdkjasd';
                $usernew->apellido='dasdasd';
                $usernew->tel='123';
                $usernew->cp='1020';
                $usernew->localidad='skdaoksdas';
                $usernew->pais='asdasda';
                $usernew->condicion_iva=null;
                $usernew->cuit=null;
                $usernew->email='asda@gmail.com';
                $usernew->aux1=null;
                $usernew->aux2=null;
                $usernew->aux3=null;
                $usernew->aux4=null;
                $usernew->aux5=null;
              
                $this->Users->save($usernew);
          
             }else{
                  $user_id=$sessionuser->read('id'); 
               
             }
             
             $this->loadModel('Sells');
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id, 'cart' => true])->all();
            

            $cantSells = count($sells);
            if($cantSells==null){
                 $cantSells=0;
            }
           
            $sell=null;
            
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
            }
            else
            {
                if($cantSells==0)
                {
                    //Si no se encuentra una venta con cart en true, se crea una nueva  
                    $sellsTable = TableRegistry::get('Sells');
                    $sell = $sellsTable->newEntity();
                    $sell->user_id = $user_id;
                    $sell->cart=true;
                    $this->Sells->save($sell);//para que genere el id automáticamente
                    
                    
                }
                else if ($cantSells==1)
                {
                    
                    $sell=$sells->first();
                    
                }                                
             
                
                $this->loadModel('Sell_items');
                $sell_itemsTable = TableRegistry::get('Sell_items');
                $sell_item = $sell_itemsTable->newEntity();
                
                $sell_item->cantidad = $cant;
                
                $this->loadModel('Combinations');
                $combination = $this->Combinations->get($combinationId);
                
                $this->loadModel('Items');
                $item = $this->Items->get($combination->item_id);
                
                $sell_item->precio = $item->price; 
                
                $sell_item->combination_id = $combinationId;
                $sell_item->sell_id = $sell->id;
               
              
                
                //guardar la venta y el item de ella
                $this->Sells->save($sell);
                $this->Sell_items->save($sell_item);

                
                
                $this->Flash->success(__('Se ha agregado este producto a su carro de compras!'));
          
          
       
            
        }
        return $this->redirect($this->referer());  
        }

    }
    
    
    public function viewCart()
    {
         $session = $this->request->session();
     
        if($this->Auth->user()!= null)
        {
            
            $user_id = $this->Auth->user('id');
           
            $this->loadModel('Sells');
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id , 'cart' => true])->all();
           
            $cantSells = count($sells);
            
            $sell=null;
            
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
                return $this->redirect($this->referer());  
            }
            else
            {
                if($cantSells==0)
                {
                    $this->Flash->error(__('The cart is empty, try adding some item to cart first'));
                    return $this->redirect($this->referer());  
                }
                else if ($cantSells==1)
                {
                 
                    $sell=$sells->first();
                }            
                
                
                $sellEager = $this->Sells->get($sell->id, [
                    'contain' => ['Users', 'SellItems.Combinations.Items']
                ]);
                
          
                //TODO actualizar precios en sell_items con el precio actual de cada prod por si es carro es viejo                 
                $this->loadModel('Items');  
               
                foreach ($sellEager->sell_items as $sellItem)
                {                    
                    $item = $this->Items->get($sellItem->combination->item->id);
                    $sellItem->precio = $item->price; // Se accede trayéndolo de nuevo con su modelo ya que prices es un campo virtual, sino, no lo trae
                }
              
             
                $this->Sells->save($sellEager);
                
                
                $this->set('cart', $sellEager);
                
              
                
            }
        }
        elseif($session->check('cart')) ////////////////////////////////////////////////////////////////////////////////////////
        {    
            $cart = $session->read('cart'); 
            
            $this->loadModel('Items');
            $this->loadModel('Combinations');
            $this->loadModel('Prices');
            
            foreach ($cart as $c) {
                
                $comb = $this->Combinations->find()->select()->where(['id' => $c['ID']])->all();
                $items_comb=$this->Items->find()->select()->where(['id' => $comb->first()->item_id])->all();
                
                $price = $this->Prices->find()->select()->where(['item_id' => $items_comb->first()->id])->order(['created' => 'DESC'])->first();
                $oferta = $price->precio_oferta!=0  && ($price->vigencia_oferta >= Time::now());
                if($oferta)
                    $price = $price->precio_oferta;        
                else
                    $price = $price->precio_lista;
                
                $sellEager['sell_items'][] = ['combination' => [
                    'item' => $items_comb->first(),
                    'talle' => $comb->first()->talle,
                    'color' => $comb->first()->color,
                    'stock' => $comb->first()->stock,
                    ],
                    'precio' => $price,
                    'cantidad' => $c['cant'],
                    'id' => $comb->first()->id
                ];     
            } 
            $this->set('cart', $sellEager);
        }        
    }
    
   public function checkout()
    { 
      $session = $this->request->session();

        if($this->Auth->user()!= null)
        {
            
            require_once(ROOT.DS.'vendor'.DS.'mercadopagoapi'.DS.'lib'.DS.'mercadopago.php');
            $mp = new MP("5242817928767641","IRnkzGjjcFsJGYHyKswvdEe8gM3n5Vi0");
            
            $user_id = $this->Auth->user('id');
             $session->write('iduser',$user_id);

            
            $this->loadModel('Sells');
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id , 'cart' => true])->all();

             $cantSells = count($sells);
            $sell=null;
            
           
        
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
                return $this->redirect($this->referer());  
            }
            else
            {
                if($cantSells==0)
                {
                    $this->Flash->error(__('The cart is empty, try adding some item to cart first'));
                    return $this->redirect($this->referer());  
                }
                else if ($cantSells==1)
                {
              
                    $sell=$sells->first();
                }            
                
                
                $sellEager = $this->Sells->get($sell->id, [
                    'contain' => ['Users', 'SellItems.Combinations.Items']
                ]);
                
         
            
              $this->loadModel('Items'); 
                 $preference_data=array('items'=>[]);
                     
                    $preciototal=0;
                foreach ($sellEager->sell_items as $sellItem)
                {                    
                   
                   $preciototal=$preciototal+($sellItem->precio*$sellItem->cantidad);
                    
                }            
          
                 $preference_data['items'][] = ["title" => "Compra de artículos en Megamoda", "id" => $sells->first()->id,"quantity" => 1,"currency_id" => "VEF", "unit_price" =>50];
    
            $preference = $mp->create_preference($preference_data);
         
      $this->redirect($preference['response']['init_point']);   
    
        
            
        }
        }
        else
        {    
            
            return $this->redirect('/login');
        }          
    }
    
      public function checkoutreceived()
    {
          
           
           header('Access-Control-Allow-Origin: *'); 
             http_response_code (200);
            $connection = ConnectionManager::get('default');
          require_once(ROOT.DS.'vendor'.DS.'mercadopagoapi'.DS.'lib'.DS.'mercadopago.php');
            $mp = new MP("5242817928767641","IRnkzGjjcFsJGYHyKswvdEe8gM3n5Vi0");
            
           if (!isset($this->request->query['id'], $this->request->query['topic']) || !ctype_digit($this->request->query['id'])) {
              http_response_code(400);
              return;
            }
               
              
        

     if($this->request->query['topic'] == 'payment'){
          $payment_info = $mp->get("/collections/notifications/".$this->request->query['id']);
          $merchant_order_info = $mp->get("/merchant_orders/".$payment_info["response"]["collection"]["merchant_order_id"]);
           $idtransaccion=$merchant_order_info["response"]["items"][0]["id"];

          $this->loadModel('Sells');
            $sellsid = $this->Sells->find()->select()->where(['id' => $idtransaccion , 'cart' => true])->all();
          
            $user_id= $sellsid->first()->user_id;
        
            $sells = $this->Sells->find()->select()->where(['user_id' => $user_id , 'cart' => true])->all();
          
         
      
        if($merchant_order_info["response"]["payments"][0]["status"]=='approved'){
            
            $cantSells = count($sells);
         
            $sell=null;
            
            if($cantSells>1)
            {
                //ERROR
                $this->Flash->error(__('Error, There is more than one cart for this user'));
                return $this->redirect($this->referer());  
            }
            else
            {
                if($cantSells==0)
                {
                    $this->Flash->error(__('The cart is empty, try adding some item to cart first'));
                    return $this->redirect($this->referer());  
                }
                else if ($cantSells==1)
                {
                  
                    $sell=$sells->first();
                }            
                
                
                $sellEager = $this->Sells->get($sell->id, [
                    'contain' => ['Users', 'SellItems.Combinations.Items']
                ]);
                
                try
                {
                    $this->loadModel('Combinations');                        
                    
                    
                    $conn = $conn = ConnectionManager::get('default');
                    $conn->begin();                    
                    
                    
                    //Por cada item descargarlo del stock            
                    foreach ($sellEager->sell_items as $sellItem)
                    {     
                        //TODO controlar si pide mas del stock y ver que ahcer
                        if($sellItem->cantidad <= $sellItem->combination->stock)
                        {
                            $combination = $this->Combinations->get($sellItem->combination->id);
                            $combination->stock = $sellItem->combination->stock - $sellItem->cantidad;
                            $this->Combinations->save($combination);
                        }
                        else
                        {
                            throw new Exception(__('Algun producto se pide en una cantidad mayor al stock, por favor elimine este producto del carro de compras y realice la comrpa por una cantidad que cumpla con el stock'));
                        }
                    }

                    //Dejar la venta como cart false
                    $sellEager->cart = false;
                    $this->Sells->save($sellEager);
                    
                    $this->set('cart', $sellEager);
                    
                    //Enviar mail al cliente
                    $this->__send_mail_to_client($sellEager); 
                    
                    //Enviar mail a la empresa para informar venta nueva
                    $this->__send_mail_to_enterprise($sellEager); 
                                        
                    
                    $conn->commit(); 
                }
                catch(Exception $e)
                {
                    //ERROR
                    $this->Flash->error($e->getMessage());
                    
                    $conn->rollback();                     
                    
                    return $this->redirect(array(
                                                'controller'=>'Cart',  
                                                'action' => 'viewcart')
                                                 );                     
                }

                //hacer en transaccion                               
            }
         }
        }
            

          
        
    }

    
    ///////////////////////////////////////////////////////////////
    ///////////////// FUNCIONES PRIVADAS///////////////////////////
    // no se corresponden con una vista
    
    private function __send_mail_to_client($sellEager)    
    {   
        // lo primero es crear el email
        $email = new Email('default');
        
        $configurations = TableRegistry::get('Configurations');
        
        $direccion_from = $configurations->find()->where(['name' => 'direccion_venta_empresa_mail'])->first()->valor;  
        $nombre_from = $configurations->find()->where(['name' => 'nombre_venta_empresa_mail'])->first()->valor;                  
        
        $email->viewVars(['title' => "Gracias por comprar en MEGAMODA su compra recibió el número identificador "]);        
        $email->viewVars(['vta_nro' => $sellEager->id]);        
        $email->viewVars(['sell' => $sellEager]);       
        $email->viewVars(['cart' => $sellEager]);       
        
        $this->set('title',"MEGAMODA");
        $this->set('sell', $sellEager);
        $this->set('cart', $sellEager);
        $this->set('_serialize', ['sell']);    
        
        
        $email
            ->template('megamodamailtemplate', 'megamodamaillayout')
            ->emailFormat('html')
            ->from([$direccion_from=> $nombre_from])
            ->to($sellEager->user->email)
            ->subject("Gracias por Comprar en MEGAMODA")
            ->send();                
        
        return $email;
    }    
            
    private function __send_mail_to_enterprise($sellEager)    
    {   
        // lo primero es crear el email
        $email = new Email('default');
        
        $configurations = TableRegistry::get('Configurations');
        
        $direccion_from = $configurations->find()->where(['name' => 'direccion_venta_empresa_mail'])->first()->valor;  
        $nombre_from = $configurations->find()->where(['name' => 'nombre_venta_empresa_mail'])->first()->valor;  
        $direccion_to = $configurations->find()->where(['name' => 'direccion_informe_ventas_mail'])->first()->valor;  
        
        
        $email->viewVars(['title' => "Nueva venta en MEGAMODA, venta número"]);        
        $email->viewVars(['vta_nro' => $sellEager->id]);        
        $email->viewVars(['sell' => $sellEager]);
        $email->viewVars(['cart' => $sellEager]);
        
        $this->set('title',"Nueva venta en MEGAMODA, venta número");
        $this->set('sell', $sellEager);
        $this->set('cart', $sellEager);
        $this->set('_serialize', ['sell']);    
                               
             
        $email
            ->template('megamodamailtemplate', 'megamodamaillayout')
            ->emailFormat('html')
            ->from([$direccion_from=> $nombre_from])
            ->to($direccion_to)
            ->subject("Nueva venta en MEGAMODA")
            ->send();                
        
        return $email;
    }     

}


