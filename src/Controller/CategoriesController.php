<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        //Se quiere todas las categorias no sola las paginadas
        $this->loadModel('Categories');        
        $this->set('categories',$this->Categories->find()->all());      
        
        $this->set('_serialize', ['categories']);
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => []
        ]);
        $this->set('category', $category);
        
        
        $this->loadModel('SubCategories');        
        $this->set('subCategories',$this->SubCategories->find()->select()->where(['category_id' => $id])->all()); 
        
        $this->set('_serialize', ['category']);
    }

}
