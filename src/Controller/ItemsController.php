<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SubCategories']
        ];
        $this->set('items', $this->paginate($this->Items));
        $this->set('_serialize', ['items']);

    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => ['SubCategories', 'Combinations', 'Prices']
        ]);
        
        $category = $item->category;
        
        //ahora queda simple porque la búsqueda del precio correcto se pasó al modelo
        
        $this->set('price', $item->price); 
        $this->set('listprice', $item->listprice); 
        $this->set('oferta', $item->oferta); 
        
        $this->set('item', $item);
        $this->set('category', $category);
        $this->set('_serialize', ['category']);
        $this->set('_serialize', ['item']);
    }
}
